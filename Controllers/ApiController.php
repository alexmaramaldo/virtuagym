<?php

namespace Controllers;
use Core\ApiBaseController;
use Core\Controller;
use Core\Notifications;
use Models\PlanDay;
use Models\PlanDayExercise;
use Models\Task;
use Models\Plan;
use Repositories\PlanDayExerciseRepository;
use Repositories\PlanDayRepository;
use Repositories\PlanRepository;
use Repositories\UserRepository;

class ApiController extends ApiBaseController
{
    var $planRepository;
    var $planDayRepository;
    var $userRepository;
    var $planModel;
    var $planDayExerciseRepository;

    public function __construct()
    {
        $this->planRepository = new PlanRepository();
        $this->planDayRepository = new PlanDayRepository();
        $this->planDayExerciseRepository = new PlanDayExerciseRepository();
        $this->userRepository = new UserRepository();
        $this->planModel = new Plan();
    }

    function plandays($plan_id)
    {
        $workDays = $this->planDayRepository->allByPlanId($plan_id);

        header('Content-type: application/json');
        echo $this->sendResponse($workDays, "Retrivied with success");
        exit;
    }

    function planday($plan_day_id)
    {
        $workDay = $this->planDayRepository->findById($plan_day_id);

        header('Content-type: application/json');
        echo $this->sendResponse($workDay, "Retrivied with success");
        exit;
    }

    public function plandaycreate()
    {
        if (empty($_POST)) {
            $_POST = json_decode(file_get_contents("php://input"), true) ? : [];
        }

        $input = $_POST;
        $data = [
            "name" => $input['name'],
            "plan_id" => $input['plan_id']
        ];

        header('Content-type: application/json');
        if ($id = $this->planDayRepository->insert($data))
        {
            $this->planDayExerciseRepository->deleteByPlanDayId($id);
            for($i = 0; $i < count($input['exercises_name']); $i++) {
                $data_exercise = [
                    "name" => $input['exercises_name'][$i]['name'],
                    "plan_day_id" => $id
                ];

                $this->planDayExerciseRepository->insert($data_exercise);
            }
            $plan = $this->planRepository->findById($input['plan_id']);
            Notifications::modifierWorkDayFromPlan($plan);

            echo $this->sendResponse($id, "Save with success");
        } else {
            echo $this->sendError("Error on save", 500);
        }
        exit;
    }

    public function plandayupdate($id)
    {
        parse_str(file_get_contents("php://input"),$_POST);

        $input = $_POST;
        $data = [
            "name" => $input['name']
        ];

        header('Content-type: application/json');
        if ($this->planDayRepository->update($id, $data))
        {
            $this->planDayExerciseRepository->deleteByPlanDayId($id);
            for($i = 0; $i < count($input['exercises_name']); $i++) {
                $data_exercise = [
                    "name" => $input['exercises_name'][$i]['name'],
                    "plan_day_id" => $id
                ];

                $this->planDayExerciseRepository->insert($data_exercise);
            }
            $planDay = $this->planDayRepository->findById($id);
            $plan = $this->planRepository->findById($planDay->plan_id);
            Notifications::modifierWorkDayFromPlan($plan);

            echo $this->sendResponse($id, "Save with success");
        } else {
            echo $this->sendError("Error on save", 500);
        }
        exit;
    }

    public function plandaydelete($id)
    {
        parse_str(file_get_contents("php://input"),$_POST);

        header('Content-type: application/json');

        $planDay = $this->planDayRepository->findById($id);

        $this->planDayExerciseRepository->deleteByPlanDayId($id);

        if ($this->planDayRepository->delete($id))
        {

            $plan = $this->planRepository->findById($planDay->plan_id);
            Notifications::modifierWorkDayFromPlan($plan);

            echo $this->sendResponse([], "Deleted with success");
        } else {
            echo $this->sendError("Error on delete", 500);
        }
        exit;
    }


}