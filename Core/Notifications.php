<?php

namespace Core;

use function getenv;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Repositories\UserRepository;

class Notifications
{

    public static function modifierWorkDayFromPlan($plan)
    {
        $subject = "You have changes in Work Days";
        $message = "You have changes in Work Days from plan: " . $plan['name'] . ', visit the system and check this!';

        $userRepository = new UserRepository();
        $users = $userRepository->getUsersByPlanId($plan['id']);

        foreach($users as $user) {
            Notifications::send($user->email, $subject, $message);
        }
    }

    public static function modifierPlan($plan)
    {
        $subject = "You have changes in your Plan";
        $message = "You have changes in your Plan: " . $plan['name'] . ', visit the system and check this!';

        $userRepository = new UserRepository();
        $users = $userRepository->getUsersByPlanId($plan['id']);

        foreach($users as $user) {
            Notifications::send($user->email, $subject, $message);
        }
    }

    public static function addedUserToPlan($email, $plan)
    {
        $subject = "New plan";
        $message = "You was added in new plan: " . $plan['name'];

        Notifications::send($email, $subject, $message);
    }

    public static function removedUserFromPlan($email, $plan)
    {
        $subject = "Removed from plan";
        $message = "You was removed from plan: " . $plan['name'];

        Notifications::send($email, $subject, $message);
    }
    public static function send($email, $subject, $message)
    {
        $mail = new PHPMailer();

        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = getenv("MAIL_HOST");
        $mail->SMTPAuth = true;
        $mail->Port = getenv("MAIL_PORT");
        $mail->Username = getenv("MAIL_USERNAME");
        $mail->Password = getenv("MAIL_PASSWORD");
        $mail->SMTPSecure = getenv("MAIL_ENCRYPTION");
        $mail->setFrom( getenv("MAIL_FROM"),  getenv("MAIL_FROM_NAME"));

        $mail->addAddress($email);
        $mail->Subject = $subject;
        $mail->Body = $message;

        if($mail->send()) {
            return true;
        }
        else {
            return false;
        }
    }
}

