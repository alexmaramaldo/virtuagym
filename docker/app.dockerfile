FROM php:7.2-fpm

RUN apt-get update && apt-get install -y \
    git \
    openssl \
    zip \
    unzip \
    libmcrypt-dev \
    libpq-dev \
    libmagickwand-dev --no-install-recommends \
    && pecl install imagick grpc \
    && docker-php-ext-enable imagick grpc \
    && docker-php-ext-install pdo mbstring pdo_pgsql pdo_mysql zip

# Install GD
RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
     && docker-php-ext-configure gd \
          --enable-gd-native-ttf \
          --with-freetype-dir=/usr/include/freetype2 \
          --with-png-dir=/usr/include \
          --with-jpeg-dir=/usr/include \
    && docker-php-ext-install gd \
    && docker-php-ext-enable gd

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
