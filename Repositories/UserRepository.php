<?php

namespace Repositories;

use Config\Database;
use Core\BaseRepository;
use Models\Task;
use Models\User;

class UserRepository extends BaseRepository
{
    var $table_name = "users";

    public function getUsersByPlanId($plan_id)
    {
        $sql = "SELECT u.* FROM users u WHERE u.plan_id = ". $plan_id ." order by u.first_name, u.last_name";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $rs = $req->fetchAll();

        $users = [];
        foreach($rs as $userRS) {
            $user = new User();
            $user->id = $userRS['id'];
            $user->first_name = $userRS['first_name'];
            $user->last_name = $userRS['last_name'];
            $user->email = $userRS['email'];
            $user->created_at = $userRS['created_at'];
            $user->updated_at = $userRS['updated_at'];

            $users[] = $user;
        }

        return $users;
    }
}
