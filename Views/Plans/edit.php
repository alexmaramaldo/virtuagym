<h1>Edit plan</h1>
<input type="hidden" class="form-control" id="plan_id" name="plan_id" value="<?php echo $plan["id"] ?>">
<form method='post' action='#'>
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="first_name" placeholder="Enter a Name" name="name" value="<?php echo (isset($plan["name"])) ? $plan["name"] : '';?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="users" class="col-sm-2 col-form-label">Work Days</label>
        <div class="col-sm-10">
            <div class="card">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" id="addPlanDay">
                    <b>+</b> Add new Work Day
                </button>
            </div>

            <br />
            <div id="workday-list">
            </div>

        </div>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>


<!-- Modal -->
<div class="modal fade" id="planDayModal" tabindex="-1" role="dialog" aria-labelledby="planDayModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="planDayModalLabel">Work Day</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="work_name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" id="work_id" name="work_id" value="">
                            <input type="text" class="form-control" id="work_name" placeholder="Enter a Name" name="work_name" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Exercises</label>
                        <div class="col-sm-10">
                            <div class="card">
                                <div class="form-row">
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="exercise_name" placeholder="Enter a Exercise" name="exercise_name" value="">
                                    </div>
                                    <div class="col">
                                        <button type="button" class="form-control btn btn-primary" id="save-exercise">
                                            <b>+</b>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="card">
                                <div class="row col-md-12 centered">
                                    <table class="table table-striped custab">
                                        <thead>
                                        Current exercises
                                        <tr>
                                            <th>Name</th>
                                            <th class="text-center">&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody id="exercises-list">
                                        <tr id="no-results">
                                            <td colspan='2'>No results</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="planday-save">Save</button>
                </div>
            </div>
        </form>
    </div>
