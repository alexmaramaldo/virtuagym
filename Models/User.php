<?php

namespace Models;

use Config\Database;
use Core\Model;

/**
 * Class User
 * @package Models
 */
class User extends Model
{
    var $id;
    var $first_name;
    var $last_name;
    var $email;
    var $plan_id;
}