<?php

namespace Models;

use Config\Database;
use Core\Model;

/**
 * Class PlanDay
 * @package Models
 */
class PlanDay extends Model
{
    var $id;
    var $plan_id;
    var $name;
    var $exercises;
}