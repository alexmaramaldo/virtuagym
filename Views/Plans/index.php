<h1>Plans</h1>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
        <a href="/plans/create" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new Plan</a>
        <tr>
            <th>Name</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
            <?php
            foreach ($plans as $plan)
            {
                echo '<tr>';
                echo "<td>" . $plan['name'] . "</td>";
                echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/plans/edit/" . $plan["id"] . "' ><i class=\"far fa-edit\"></i></a> <a href='/plans/delete/" . $plan["id"] . "' class='btn btn-danger btn-xs' onclick=\"return confirm('Are you sure?')\"><i class=\"fas fa-trash\"></i></a></td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>