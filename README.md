# Alex Maramaldo #virtuagym!

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

# Before everything
I created this project in 2 days, because from my current work have a lot of problems this week, I tried using a little bit for the most part important from my skills, the system has particular parts purposeful, per example, Models with public vars, without use getter's and setter's, I won time in this case, I don't used TDD with PHPUNIT in this case because I don't have more time, but, if you need, tell me :), I can to create this without problem! I tested my project in Google Chrome and Firefox, and, with this, I can certify the layout. If you have any questions, don't leave call me, I can help you to understand my idea, Hugs and I hope to see you coming soon!

To layout, I created a simple interface to interact with the user!

# Tech specification
The project uses `PHP 7.2` and `MySQL`

*Folder structure*


    .
    ├── ...
    ├── Config          # Folder with db.php
    ├── Controllers     # All controllers from projects
    ├── Core            # Base classes and parent classes
    ├── docker          # Structure to run docker-compose
    ├── Models          # All models from project
    ├── Repositories    # All repositories from projects, responsible to manipulate data inside of MySQL
    ├── vendor          # Generate by composer.json
    ├── Views           # Folder with views from project
    │   ├── Layouts     # Layout from project
    │   ├── Plans       # Layout from plans
    │   └── Users       # Default Layout from users
    ├── Webroot         # Default Web Root from project
    │   └── assets      # Default assets from users
    │       ├── css     # With css files
    │       └── js      # With main JS files



# Demo video

[![Demo video](https://img.youtube.com/vi/5qVAZzLM4YE/0.jpg)](https://www.youtube.com/watch?v=5qVAZzLM4YE)

# Description
1. The system was created to register Plans with Day's and exercises by day, and after this, you can to put a plan to a user or more. I have a example running here: `http://virtuagym.alexmaramaldo.com/`
2. Created using PHP from scratch, I don't used a PHP Framework, I created literally my owner framework to this Code Challenge, using MVC structure, with Repositories, Router, Dispatchers and others, I hope you like this.
3. The project has Webroot, this is the ROOT folder, you need to appointment your server to here.
4. The project has a `dump_mysql.sql` file in root, you need import before to run.
5. The project has `.env.example`, you need to create a copy of this file to `.env` and set the values to the environment.
6. I used the composer to load my classes and, the only framework used here is PHPMailer, to send emails, and phpdotenv to load envvars.
7. If you have `docker compose` and you want to use:
	- After to create .env, you need to run: `docker-compose up` 
	- and after, you will have MySQL running in `127.0.0.1`, port `3306`, user: `root`, password: `root` and database: `homestead`
	- You will need to import `dump_mysql.sql`.
	- You access in url `http://localhost:8000`

> (*) If you have a problem to run this, you can contact directly me in e-mail: `alexmaramaldo@gmail.com`

# Requirements if you need to create a custom server:
If you need put the project inside linux server, you need to install APACHE 2 or NGINX or Similar, but you need PHP 7.2 and sub modules and MySQL or MariaDB(preferably).
Example command with apache::
```sh
$ sudo apt-get update && apt-get -y upgrade && apt-get -y install \
  apache2 php7.2 libapache2-mod-php7.2 curl php7.2-curl php7.2-simplexml php7.2-mysql mysql-server && a2enmod php7.2
```
And point the root directory to folder Webroot
