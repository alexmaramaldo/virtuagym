<?php

namespace Repositories;

use Config\Database;
use Core\BaseRepository;
use Models\PlanDay;
use Models\PlanDayExercise;
use Models\Task;
use Models\Plan;

class PlanDayExerciseRepository extends BaseRepository
{
    var $table_name = "plan_day_exercises";

    public function allByPlanDayId($plan_day_id)
    {
        $sql = "SELECT * FROM ".$this->table_name." WHERE plan_day_id = " . $plan_day_id;

        $req = Database::getBdd()->prepare($sql);
        $req->execute();

        $rs = $req->fetchAll();

        $planDayExercises = [];
        foreach($rs as $tmp) {
            $planDayExercise = new PlanDayExercise();
            $planDayExercise->id = $tmp['id'];
            $planDayExercise->name = $tmp['name'];
            $planDayExercise->created_at = $tmp['created_at'];
            $planDayExercise->updated_at = $tmp['updated_at'];

            $planDayExercises[] = $planDayExercise;
        }
        return $planDayExercises;
    }

    public function deleteByPlanDayId($plan_day_id)
    {
        $sql = "DELETE FROM ".$this->table_name." WHERE plan_day_id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$plan_day_id]);
    }

}