<?php

namespace Repositories;

use Config\Database;
use Core\BaseRepository;
use Models\PlanDay;
use Models\Task;
use Models\Plan;

class PlanDayRepository extends BaseRepository
{
    var $table_name = "plan_days";

    public function allByPlanId($plan_id)
    {
        $sql = "SELECT * FROM ".$this->table_name." WHERE plan_id = " . $plan_id;

        $req = Database::getBdd()->prepare($sql);
        $req->execute();

        $rs = $req->fetchAll();

        $planDayExerciseRepository = new PlanDayExerciseRepository();

        $planDays = [];
        foreach($rs as $tmp) {
            $planDay = new PlanDay();
            $planDay->id = $tmp['id'];
            $planDay->name = $tmp['name'];
            $planDay->plan_id = $tmp['plan_id'];
            $planDay->exercises = $planDayExerciseRepository->allByPlanDayId($tmp['id']);
            $planDay->created_at = $tmp['created_at'];
            $planDay->updated_at = $tmp['updated_at'];

            $planDays[] = $planDay;
        }
        return $planDays;
    }

    public function findById($id)
    {
        $sql = "SELECT * FROM ".$this->table_name." WHERE id =" . $id;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $rs = $req->fetch();

        $planDayExerciseRepository = new PlanDayExerciseRepository();

        $planDay = new PlanDay();
        $planDay->id = $rs['id'];
        $planDay->name = $rs['name'];
        $planDay->plan_id = $rs['plan_id'];
        $planDay->exercises = $planDayExerciseRepository->allByPlanDayId($rs['id']);
        $planDay->created_at = $rs['created_at'];
        $planDay->updated_at = $rs['updated_at'];

        return $planDay;
    }

}
