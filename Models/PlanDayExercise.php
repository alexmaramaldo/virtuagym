<?php

namespace Models;

use Config\Database;
use Core\Model;

/**
 * Class PlanDayExercise
 * @package Models
 */
class PlanDayExercise extends Model
{
    var $id;
    var $name;
}