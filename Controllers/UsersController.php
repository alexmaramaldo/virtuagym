<?php

namespace Controllers;
use Core\Controller;
use Core\Notifications;
use Models\Plan;
use Models\Task;
use Models\User;
use Repositories\PlanRepository;
use Repositories\UserRepository;

class UsersController extends Controller
{
    var $userRepository;
    var $planRepository;
    var $userModel;
    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userModel = new User();
        $this->planRepository = new PlanRepository();
    }

    function index()
    {
        $d['users'] = $this->userRepository->all();
        $this->set($d);
        $this->render("index");
    }

    function create()
    {
        if ($_POST)
        {
            $input = $_POST;

            $data = [
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'plan_id' => (!empty($input['plan_id'])) ? $input['plan_id'] : null,
            ];

            if ($this->userRepository->insert($data))
            {
                if(!empty($input['plan_id'])) {
                    $plan = $this->planRepository->findById($input['plan_id']);
                    Notifications::addedUserToPlan($input['email'], $plan);
                }
                header("Location: /users/index");
            }
        }

        $d['plans'] = $this->planRepository->all();
        $this->set($d);

        $this->render("create");
    }

    function edit($id)
    {
        if ($_POST)
        {
            $input = $_POST;

            $old_plan_id = $this->userRepository->findById($id)['plan_id'];
            $data = [
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'plan_id' => (!empty($input['plan_id'])) ? $input['plan_id'] : null,
            ];

            if ($this->userRepository->update($id, $data))
            {
                if(!empty($input['plan_id'])) {
                    if($old_plan_id != $input['plan_id']) {
                        $plan = $this->planRepository->findById($input['plan_id']);
                        Notifications::addedUserToPlan($input['email'], $plan);
                    }
                } else {
                    if($old_plan_id > 0) {
                        $plan = $this->planRepository->findById($old_plan_id);
                        Notifications::removedUserFromPlan($input['email'], $plan);
                    }
                }

                header("Location: /users/index");
            }
        }

        $d["user"] = $this->userRepository->findById($id);
        $d['plans'] = $this->planRepository->all();
        $this->set($d);

        $this->set($d);
        $this->render("edit");
    }

    function delete($id)
    {
        if ($this->userRepository->delete($id))
        {
            header("Location: /users/index");
        }
    }
}