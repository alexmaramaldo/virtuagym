<h1>Create user</h1>
<form method='post' action='#'>
    <div class="form-group row">
        <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="first_name" placeholder="Enter a First Name" name="first_name">
        </div>
    </div>

    <div class="form-group row">
        <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="last_name" placeholder="Enter a Last Name" name="last_name">
        </div>

    </div>

    <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" placeholder="Enter an Email" name="email">
        </div>
    </div>

    <div class="form-group row">
        <label for="users" class="col-sm-2 col-form-label">Plan</label>
        <div class="col-sm-10">
            <select class="form-control js-select-multiple" name="plan_id" id="plan_id">
                <option value="">Empty</option>
                <?php foreach($plans as $plan) { ?>
                    <option  value="<?php echo $plan['id'] ?>"><?php echo $plan['name']  ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>