<h1>Users</h1>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
        <a href="/users/create" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new User</a>
        <tr>
            <th>Name</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
            <?php
            foreach ($users as $user)
            {
                echo '<tr>';
                echo "<td>" . $user['first_name'] . " " . $user['last_name'] . "</td>";
                echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/users/edit/" . $user["id"] . "' ><i class=\"fas fa-user-edit\"></i></a> <a href='/users/delete/" . $user["id"] . "' class='btn btn-danger btn-xs' onclick=\"return confirm('Are you sure?')\"><i class=\"fas fa-trash\"></i></a></td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>