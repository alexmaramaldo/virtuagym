<?php

namespace Repositories;

use Config\Database;
use Core\BaseRepository;
use Models\Task;
use Models\Plan;
use Models\User;

class PlanRepository extends BaseRepository
{
    var $table_name = "plans";

}
