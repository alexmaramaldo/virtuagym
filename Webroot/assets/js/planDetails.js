function loadPlanDays(planId) {
    $.ajax({
        url: "/api/plandays/" + planId,
        method: "GET",
    }).done(function( data ) {
        if(data.success === true) {
            var workDays = data.data;
            var stringHtml = "";
            for(i = 0; i < workDays.length; i++) {
                stringHtml += '<div class="card" data-id="' + workDays[i].id +'">';
                stringHtml += '<div class="card-body" >';
                stringHtml += '<div class="form-row">';
                stringHtml += '<div class="col-sm-10"><a href="javascript:void(0)" onclick="editPlanDay(' + workDays[i].id +')">' + workDays[i].name + '</a></div>';
                stringHtml += '<div><a href="javascript:void(0)" class="btn btn-danger btn-xs delete-exercise" onclick="removePlanDay(' + workDays[i].id +')"><i class="fas fa-trash"></i></a></div>';
                stringHtml += '</div>';
                stringHtml += '</div>';
                stringHtml += '</div>';
            }

            $('#workday-list').html(stringHtml);
        } else {
            console.log(2);
        }
    }).fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });
}

function editPlanDay(planDayId) {

    getPlanDayById(planDayId);
    $('#planDayModal').modal('show');
}

function createPlanDay() {

    $("#work_id").val("0");
    $("#work_name").val("");
    $("#exercise_name").val("");

    stringHtml = "";
    stringHtml += '<tr id="no-results">';
    stringHtml += "<td colspan='2'>No results</td>";
    stringHtml += "</tr>";

    $('#exercises-list').html(stringHtml);

    $('#planDayModal').modal('show');
}

function removePlanDay(planDayId){
    if(confirm('Are you sure?')) {
        $.ajax({
            url: "/api/plandaydelete/" + planDayId,
            method: "DELETE",
        }).done(function( data ) {
            if(data.success === true) {
                loadPlanDays($("#plan_id").val());
            } else {
                console.log(2);
            }
        }).fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });
    }
}

function formatExercisesNames(){
    var obj = [];
    $('input[name^="exercises_name"]').each(function() {
        var value = $(this).val();
        obj.push({
            name: value
        })
    });
    return obj;
}

function getPlanDayById(planDayId) {
    $.ajax({
        url: "/api/planday/" + planDayId,
        method: "GET",
    }).done(function( data ) {
        if(data.success === true) {
            var planDay = data.data;
            var stringHtml = "";

            $("#work_id").val(planDay.id);
            $("#work_name").val(planDay.name);

            if(planDay.exercises.length > 0) {
                for(i = 0; i < planDay.exercises.length; i++) {
                    stringHtml += '<tr>';
                    stringHtml += '<input type="hidden" name="exercises_name[]" value="' + planDay.exercises[i].name +'" />';
                    stringHtml += "<td>" + planDay.exercises[i].name + "</td>";
                    stringHtml += '<td class="text-center">' +
                        '<a href="javascript:void(0)" class="btn btn-danger btn-xs delete-exercise" onclick="removeExercise($(this))">' +
                        '<i class="fas fa-trash"></i></a></td>';
                    stringHtml += "</tr>";
                }
            } else {
                stringHtml = "";
                stringHtml += '<tr id="no-results">';
                stringHtml += "<td colspan='2'>No results</td>";
                stringHtml += "</tr>";
            }

            $('#exercises-list').html(stringHtml);

        } else {
            console.log(2);
        }
    }).fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });
}

function postPlanDayData() {
    var objTotal = formatExercisesNames();
    if(objTotal.length == 0) {
        alert('You need inform the minimum 1 exercise!');
        return false;
    }

    $.ajax({
        url: "/api/plandaycreate",
        method: "POST",
        type: 'json',
        data: {
            name: $("#work_name").val(),
            plan_id: $("#plan_id").val(),
            exercises_name: formatExercisesNames(),
        }
    }).done(function( data ) {
        if(data.success === true) {
            loadPlanDays($("#plan_id").val());
            $('#planDayModal').modal('hide');
        } else {
            console.log(2);
        }
    }).fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });
}

function putPlanDayData() {
    var objTotal = formatExercisesNames();
    if(objTotal.length == 0) {
        alert('You need inform the minimum 1 exercise!');
        return false;
    }

    $.ajax({
        url: "/api/plandayupdate/" + $("#work_id").val(),
        method: "PUT",
        type: 'json',
        data: {
            name: $("#work_name").val(),
            exercises_name: formatExercisesNames(),
        }
    }).done(function( data ) {
        if(data.success === true) {
            loadPlanDays($("#plan_id").val());
            $('#planDayModal').modal('hide');
        } else {
            console.log(2);
        }
    }).fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });
}

$(document).ready(function(){
    $.ajaxSetup({ cache: false });

    if($("#plan_id").val()) {
        loadPlanDays($("#plan_id").val());
    }

    $("#addPlanDay").on('click', function(){
        createPlanDay();
    });

    $("#planday-save").on('click', function(){
        if($("#work_id").val() == "0") {
            postPlanDayData();
        } else {
            putPlanDayData();
        }

    });

    $("#save-exercise").on('click', function(){
        if(!$("#exercise_name").val()) {
            alert("You need to inform a valid exercise!")
        } else {
            $("#no-results").remove();
            stringHtml = "";
            stringHtml += '<tr>';
            stringHtml += '<input type="hidden" name="exercises_name[]" value="' + $("#exercise_name").val() +'" />';
            stringHtml += "<td>" + $("#exercise_name").val() + "</td>";
            stringHtml += '<td class="text-center">' +
                '<a href="javascript:void(0)" data-name="' + $("#exercise_name").val() + '" onclick="removeExercise($(this))" class="btn btn-danger btn-xs delete-exercise">' +
                '<i class="fas fa-trash"></i></a></td>';
            stringHtml += "</tr>";

            $('#exercises-list').prepend(stringHtml);

            $("#exercise_name").val("");

        }

    });
});



function removeExercise(editButton) {

    if(confirm('Are you sure?')) {
        editButton.parents('tr')[0].remove();
    }

    var totalTr = $("#exercises-list").find("tr").length;
    if(totalTr == 0) {
        stringHtml = "";
        stringHtml += '<tr id="no-results">';
        stringHtml += "<td colspan='2'>No results</td>";
        stringHtml += "</tr>";

        $('#exercises-list').html(stringHtml);
    }

    return false;
}
