<?php

namespace Controllers;
use Core\Controller;
use Core\Notifications;
use Models\Task;
use Models\Plan;
use Repositories\PlanRepository;
use Repositories\UserRepository;

class PlansController extends Controller
{
    var $planRepository;
    var $userRepository;
    var $planModel;
    public function __construct()
    {
        $this->planRepository = new PlanRepository();
        $this->userRepository = new UserRepository();
        $this->planModel = new Plan();
    }

    function index()
    {
        $d['plans'] = $this->planRepository->all();

        $this->set($d);
        $this->render("index");
    }

    function create()
    {
        if ($_POST)
        {
            $input = $_POST;
            $data = [
                'name' => $input['name']
            ];
            if ($id = $this->planRepository->insert($data))
            {
                header("Location: /plans/edit/" . $id);
            }
        }

        $this->render("create");
    }

    function edit($id)
    {
        $d["plan"] = $this->planRepository->findById($id);

        if ($_POST)
        {
            $input = $_POST;
            $data = [
                'name' => $input['name']
            ];

            if ($this->planRepository->update($id, $data))
            {
                Notifications::modifierPlan($d["plan"]);
                header("Location: /plans/index");
            }
        }

//        $d['current_users'] = $this->planRepository->getUsersByPlanId($id);

        $this->set($d);
        $this->render("edit");
    }

    function delete($id)
    {
        $plan = $this->planRepository->findById($id);
        Notifications::modifierPlan($plan);
        if ($this->planRepository->delete($id))
        {
            header("Location: /plans/index");
        }
    }
}